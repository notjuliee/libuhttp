/**
 * @file main.cpp
 * @author Julia Ahopelto <julia@alt.icu>
 * @date 5/21/19
 * @breif Short program to demonstrate the functionality
 */

#include <uhttp/Modules/BaseModule.h>
#include <uhttp/Server/Server.h>

using namespace uhttp;

//! [Creating a module]
class IndexModule : public BaseModule<IndexModule> {
  public:
    IndexModule() {
        //! [Registering a route]
        Get("/", &IndexModule::index);
        Get("/user/{id:\\d+}", &IndexModule::user);
        Post("/post", &IndexModule::postRoute);
        //! [Registering a route]
    }

  protected:
    Response index() { return {"<h1>Hello, World!</h1>", "text/html"}; }

    Response user(int id) { return {std::to_string(id)}; }

    Response postRoute(const Request &req) { return req.body(); }
};
//! [Creating a module]

int main() {
    Server server;
    server.install<IndexModule>();

    server.run();

    return 0;
}
