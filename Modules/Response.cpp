#include "uhttp/Modules/Response.h"
#include "uhttp/Modules/Request.h"

#include <fcgiapp.h>

namespace uhttp {

void Response::write(FCGX_Stream *stream) noexcept {
    FCGX_PutS("Status: ", stream);
    auto status = std::to_string(m_status);
    FCGX_PutS(status.c_str(), stream);
    FCGX_PutS("\r\nContent-Type: ", stream);
    FCGX_PutS(m_type.c_str(), stream);
    FCGX_PutS("\r\n\r\n", stream);
    FCGX_PutS(m_body.c_str(), stream);
}

} // namespace uhttp
