#include "uhttp/Modules/Request.h"

#include <boost/lexical_cast.hpp>
#include <fcgiapp.h>
#include <r3/r3.h>
#include <stdio.h>

#include "uhttp/Modules/Response.h"

// Only needs to be unique for every HTTP method
constexpr unsigned int garbage_hash(const char *str) {
    unsigned int hash = 0;

    for (size_t i = 0; str[i] != '\0'; i++) {
        hash += str[i];
    }

    return hash;
}

namespace uhttp {

const char *Request::param(const char *name) const noexcept { return FCGX_GetParam(name, m_req->envp); }

int Request::method() const noexcept {
#define M_CASE(x)                                                                                                      \
    case garbage_hash(#x):                                                                                             \
        return METHOD_##x

    switch (garbage_hash(param("REQUEST_METHOD"))) {
        M_CASE(GET);
        M_CASE(POST);
        M_CASE(PUT);
        M_CASE(DELETE);
        M_CASE(PATCH);
        M_CASE(HEAD);
        M_CASE(OPTIONS);
    default:
        return 0;
    }
#undef M_CASE
}

void Request::prinenv() const {
    puts("\n\nPrinting environment");
    char **env = m_req->envp;
    while (*(++env))
        puts(*env);
}

void Request::load() {
    m_body.clear();
    auto size_s = param("CONTENT_LENGTH");
    if (size_s && *size_s != '\0') {
        int body_si = boost::lexical_cast<int>(size_s);
        m_body.resize(body_si);
        FCGX_GetStr(&m_body[0], body_si, m_req->in);
    }
}

} // namespace uhttp
