#include "uhttp/Server/Server.h"

#include <fcgiapp.h>
#include <r3/r3.hpp>
#ifdef __unix__
#include <sys/stat.h>
#endif

#include "uhttp/Modules/BaseModule.h"

namespace uhttp {

Server::Server(const char *path, int backlog) {
    m_socket = FCGX_OpenSocket(path, backlog);
    m_tree = std::make_unique<r3::Tree>(8);
#ifdef __unix__
    // Make sure everyone can talk to the socket
    if (access(path, F_OK) != -1) {
        chmod(path, 0777);
    }
#endif
}

void Server::run() {
    m_tree->compile();

    FCGX_Request f_req;
    Request req(&f_req);

    FCGX_Init();
    FCGX_InitRequest(&f_req, m_socket, 0);

    while (FCGX_Accept_r(&f_req) == 0) {
        r3::MatchEntry match(req.uri(), strlen(req.uri()));
        match.set_request_method(req.method());

        r3::Route matched = m_tree->match_route(match);
        if (!matched) {
            // TODO: Installable handle for this
            Response res{"Not found", "text/plain", 404};
            res.write(req.m_req->out);
            continue;
        }

        // We don't actually need any of this, defer loading it until we're sure there's a handler
        try {
            req.load();
        } catch (std::exception &e) {
            Response{e.what(), "text/plain", 400}.write(req.m_req->out);
            continue;
        }
        auto handler = *static_cast<Handler *>(matched.data());
        try {
            auto res = handler(req, match.get()->vars);
            res.write(req.m_req->out);
        } catch (std::exception &e) {
            Response{e.what(), "text/plain", 500}.write(req.m_req->out);
            continue;
        }
    }
}

void Server::install_mod(std::unique_ptr<uhttp::IBaseModule> &mod) noexcept { mod->install(*m_tree); }

} // namespace uhttp
