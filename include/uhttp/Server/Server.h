#ifndef LIBUHTTP_SERVER_H
#define LIBUHTTP_SERVER_H

#include <boost/any.hpp>
#include <memory>
#include <vector>

namespace r3 {
class Tree;
}

namespace uhttp {

template <typename> class BaseModule;
class IBaseModule;

class Server {
  public:
    explicit Server(const char *path = "/tmp/fcgiapp.sock", int backlog = 10);

    template <typename T, typename... Args> void install(Args... args) noexcept {
        m_modules.push_back(std::unique_ptr<IBaseModule>(new T(args...)));
        install_mod(m_modules.back());
    }

    void run();

  private:
    void install_mod(std::unique_ptr<IBaseModule> &mod) noexcept;

    int m_socket;
    std::unique_ptr<r3::Tree> m_tree;
    std::vector<std::unique_ptr<IBaseModule>> m_modules;
};

} // namespace uhttp

#endif // LIBUHTTP_SERVER_H
