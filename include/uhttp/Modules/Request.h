#ifndef LIBUHTTP_REQUEST_H
#define LIBUHTTP_REQUEST_H

#include <string>

struct FCGX_Request;

namespace uhttp {

class Response;

class Request {
    friend Response;

  public:
    explicit Request(FCGX_Request *req) : m_req(req) {}

    const char *param(const char *name) const noexcept;
    const char *uri() const noexcept { return param("REQUEST_URI"); }
    const std::string &body() const noexcept { return m_body; }

    int method() const noexcept;

    void prinenv() const;

  private:
    friend class Server;

    void load();

  private:
    FCGX_Request *m_req;
    std::string m_body;
};

} // namespace uhttp

#endif // LIBUHTTP_REQUEST_H
