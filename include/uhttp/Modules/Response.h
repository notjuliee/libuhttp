#ifndef LIBUHTTP_RESPONSE_H
#define LIBUHTTP_RESPONSE_H

#include <string>
#include <utility>

struct FCGX_Stream;

namespace uhttp {

class Request;

class Response {
  public:
    Response(const std::string &body = "", const std::string &type = "text/plain", int status = 200)
        : m_body(body), m_type(type), m_status(status) {}

  private:
    friend class Server;
    void write(FCGX_Stream *stream) noexcept;

  private:
    std::string m_body, m_type;
    int m_status;
};

} // namespace uhttp

#endif // LIBUHTTP_RESPONSE_H
