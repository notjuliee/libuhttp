/**
 * @file BaseModule.h
 * @author Julia Ahopelto <julia@alt.icu>
 * @date 5/21/19
 * @breif Base module class
 */

#ifndef LIBUHTTP_BASEMODULE_HPP
#define LIBUHTTP_BASEMODULE_HPP

#include <boost/lexical_cast.hpp>
#include <functional>
// TODO: Begone foul demon
#include <iostream>
#include <r3/r3.hpp>
#include <tuple>
#include <vector>

#include "Request.h"
#include "Response.h"

namespace uhttp {

using Handler = std::function<Response(const Request &req, const str_array &)>;

/**
 * Internal interface to allow storage of modules
 */
class IBaseModule {
  public:
    virtual ~IBaseModule() = default;

  private:
    friend class Server;
    virtual void install(r3::Tree &tree) noexcept = 0;
};

/**
 * The base class for all HTTP modules
 *
 * Creating a module:
 * \snippet main.cpp Creating a module
 *
 * @tparam T The inherited class
 */
template <class T> class BaseModule : public IBaseModule {
  protected:
    /**
     * Register a route for a module
     *
     * If you wish to access the Request object, the first parameter _MUST_
     * be of the type `const Request&`
     *
     * \snippet main.cpp Registering a route
     *
     * @param method HTTP method bitmask
     * @param pattern r3 route pattern
     * @param fptr Pointer to a member function that will be called
     */
    template <typename... Args> void Route(int method, const char *pattern, Response (T::*fptr)(Args...)) noexcept {
        _handlers.emplace_back(method, pattern, [this, fptr](const Request &req, const str_array &params) {
            return callFunc(fptr, std::index_sequence_for<Args...>{}, params);
        });
    }

    template <typename... Args>
    void Route(int method, const char *pattern, Response (T::*fptr)(const Request &, Args...)) noexcept {
        _handlers.emplace_back(method, pattern, [this, fptr](const Request &req, const str_array &params) {
            return callFunc(fptr, std::index_sequence_for<Args...>{}, params, req);
        });
    }

    // Wrapper to allow common HTTP methods
    template <typename... Args> void Get(const char *pattern, Response (T::*fptr)(Args...)) noexcept {
        Route(METHOD_GET, pattern, fptr);
    }
    template <typename... Args> void Post(const char *pattern, Response (T::*fptr)(Args...)) noexcept {
        Route(METHOD_POST, pattern, fptr);
    }

  private:
    void install(r3::Tree &tree) noexcept override {
        for (auto &x : _handlers) {
            const char *path = std::get<1>(x);
            tree.insert_routel(std::get<0>(x), path, strlen(path), &std::get<2>(x));
        }
    }

    std::vector<std::tuple<int, const char *, Handler>> _handlers;

  private:
    template <typename... Args, std::size_t... Is>
    Response callFunc(Response (T::*fptr)(Args...), std::index_sequence<Is...>, const str_array &params) {
        return (static_cast<T *>(this)->*fptr)(boost::lexical_cast<Args>(params.tokens.entries[Is].base)...);
    }
    template <typename... Args, std::size_t... Is>
    Response callFunc(Response (T::*fptr)(const Request &, Args...), std::index_sequence<Is...>,
                      const str_array &params, const Request &req) {
        return (static_cast<T *>(this)->*fptr)(req, boost::lexical_cast<Args>(params.tokens.entries[Is].base)...);
    }

    BaseModule(){};
    friend T; // Make sure the correct derived class was passed
};

} // namespace uhttp

#endif // LIBUHTTP_BASEMODULE_HPP
